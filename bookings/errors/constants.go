package errors

const (
	NotAvailableClass         = "class is not available in this day"
	AlreadyBookedByThisPerson = "class is not booked by this person"
	NameIsEmpty               = "name is empty"
	InvalidDate               = "date is invalid"
)
