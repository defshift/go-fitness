package entities

import (
	"cloud.google.com/go/civil"
	constants "go-fitness/bookings/errors"
	"go-fitness/common/errors"
)

type Booking struct {
	Name string     `json:"name"`
	Date civil.Date `json:"date"`
}

func (b *Booking) Validate() error {
	if b.Name == "" {
		return &errors.ValidationError{Message: constants.NameIsEmpty}
	}

	if b.Date.Year == 0 {
		return &errors.ValidationError{Message: constants.InvalidDate}
	}

	return nil
}
