package common

type ErrorResponse struct {
	Message string `json:"name"`
	Details string `json:"details"`
}
