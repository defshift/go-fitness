package main

import (
	"github.com/gofiber/fiber"
	"github.com/gofiber/fiber/middleware"
	"go-fitness/bookings"
	bookingEntities "go-fitness/bookings/entities"
	"go-fitness/classes"
	classEntities "go-fitness/classes/entities"
	"go-fitness/common"
	"go-fitness/storage"
	"log"
	"net/http"
	"os"
	"strconv"
)

const defaultHttpPort = 3000

type app struct {
	server       *fiber.App
	classStorage storage.Storage
}

func New() *app {
	s := storage.New()
	classHandler := classes.New(s)
	bookingHandler := bookings.New(s)

	server := fiber.New(&fiber.Settings{
		Prefork:      false,
		ServerHeader: "fitness-app",
	})

	server.Use(middleware.Logger())
	server.Get("/", func(c *fiber.Ctx) {
		c.Send("fitness-app v1.0")
	})

	server.Post("/classes", common.UnmarshalAndValidateMiddleware(&classEntities.Class{}), classHandler.Create())
	server.All("/classes", func(c *fiber.Ctx) {
		c.Status(http.StatusMethodNotAllowed)
	})

	server.Post("/bookings", common.UnmarshalAndValidateMiddleware(&bookingEntities.Booking{}), bookingHandler.Create())
	server.All("/bookings", func(c *fiber.Ctx) {
		c.Status(http.StatusMethodNotAllowed)
	})

	// not found for other routes
	server.Use(func(c *fiber.Ctx) {
		c.SendStatus(404)
	})

	a := &app{
		server:       server,
		classStorage: storage.New(),
	}

	return a
}

func (a *app) Start() error {
	port := defaultHttpPort
	if envPort, exists := os.LookupEnv("PORT"); exists != false {
		var err error
		port, err = strconv.Atoi(envPort)

		if err != nil {
			log.Fatalf("%v", err)
		}
	}

	return a.server.Listen("0.0.0.0:" + strconv.Itoa(port))
}

func (a *app) Stop() error {
	return a.server.Shutdown()
}
