# About
A simple service that allows to create classes and book them

## Installation
### Using Docker
The simplest way to start the service is docker:

Clone the repo and build the image:

```
docker build -t fitness-app .
```

Run the app:

```
docker run -it --rm -p 3000:3000 --name some-fitness-app fitness-app
```

Check `http://localhost:3000` in your browser to make sure the service is healthy.

### Local build

(Requirements: Go >= 1.4)

Clone the repo and build the app:

```
go build .
```

Run the app:

```
./go-fitness
```

Check `http://localhost:3000` in your browser to make sure the service is healthy.

## Tests

The project contains only integration tests placed in the `app_test.go` file. It covers most of the codebase.

To run tests locally:

```$go
go test -v ./...
```

#### Env vars

The `PORT` env var is available to customize server http port. 3000 by default.

## Usage

The service provides two endpoints:


### Creating classes
**POST** http://localhost:3000/classes

example body: 
```
{ "name":  "pilatos", "startDate": "2006-01-02", "endDate":  "2006-01-02", "capacity":  1 }
```

Returns 201 http code if the class has been created. If date is not available or body is not valid 400 code is returned with an error message.


### Booking classes
**POST** http://localhost:3000/bookings

example body: 
```
{ "name":  "John", "date": "2006-01-02" }
```

Returns 201 http code if the booking has been created. If class is not available or body is not valid 400 code is returned with an error message.

### rest.http

The repo contains `rest.http` file that allows to make rest calls locally using Intellij Idea (Goland, etc.).

## Trade-offs, TODOs

1. Standard json decoder, encoder is used. Could be replaced by code-generated solution.
2. `context` is not use in the storage layer.
3. `content-type` is always considered as json
4. Comments/Docs are not complete