package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	app := New()

	go func() {
		err := app.Start()

		if err != nil {
			log.Fatalf("%v", err)
		}
	}()

	// Check for a closing signal
	// Graceful shutdown
	sigquit := make(chan os.Signal, 1)
	signal.Notify(sigquit, os.Interrupt, syscall.SIGTERM)

	sig := <-sigquit
	log.Printf("caught sig: %+v", sig)
	log.Printf("Gracefully shutting down server...")

	if err := app.Stop(); err != nil {
		log.Printf("Unable to shut down server: %v", err)
	} else {
		log.Println("Server stopped")
	}
}
