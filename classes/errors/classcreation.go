package errors

import "fmt"

type ClassCreationError struct {
	Message string
}

func (e *ClassCreationError) Error() string {
	return fmt.Sprintf("classcreation: %s", e.Message)
}
