package errors

const (
	EndDateBeforeStart = "end date is after the start date"
	BusySlots          = "not possible to create class due the busy slots"
	NameIsEmpty        = "name is empty"
	InvalidStartDate   = "start date is not valid"
	InvalidEndDate     = "end date is not valid"
	InvalidCapacity    = "capacity is not valid"
)
