# builder image
FROM golang as builder

ADD . /src

WORKDIR /src

RUN go get -d -v ./...
RUN go install -v ./...

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags '-w -s' -a -installsuffix cgo -o fitness-app .

# image with the artifact
FROM scratch

ENV PORT 3000

COPY --from=builder /src /app/

WORKDIR /app

CMD ["./fitness-app"]